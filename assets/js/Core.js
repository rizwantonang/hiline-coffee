/*JSON*/
if(typeof JSON!=='object'){JSON={};}
(function(){'use strict';function f(n){return n<10?'0'+n:n;}
if(typeof Date.prototype.toJSON!=='function'){Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+'-'+
f(this.getUTCMonth()+1)+'-'+
f(this.getUTCDate())+'T'+
f(this.getUTCHours())+':'+
f(this.getUTCMinutes())+':'+
f(this.getUTCSeconds())+'Z':null;};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf();};}
var cx,escapable,gap,indent,meta,rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==='string'?c:'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);})+'"':'"'+string+'"';}
function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key);}
if(typeof rep==='function'){value=rep.call(holder,key,value);}
switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null';}
gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==='[object Array]'){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null';}
v=partial.length===0?'[]':gap?'[\n'+gap+partial.join(',\n'+gap)+'\n'+mind+']':'['+partial.join(',')+']';gap=mind;return v;}
if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){if(typeof rep[i]==='string'){k=rep[i];v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}else{for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}
v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+mind+'}':'{'+partial.join(',')+'}';gap=mind;return v;}}
if(typeof JSON.stringify!=='function'){escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'};JSON.stringify=function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' ';}}else if(typeof space==='string'){indent=space;}
rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('JSON.stringify');}
return str('',{'':value});};}
if(typeof JSON.parse!=='function'){cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;JSON.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v;}else{delete value[k];}}}}
return reviver.call(holder,key,value);}
text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+
('0000'+a.charCodeAt(0).toString(16)).slice(-4);});}
if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j;}
throw new SyntaxError('JSON.parse');};}}());

/*! jQuery Cookie Plugin v1.4.1 https://github.com/carhartl/jquery-cookie Copyright 2013 Klaus Hartl Released under the MIT license */
!function(e){"function"==typeof define&&define.amd?define(["jquery"],e):e("object"==typeof exports?require("jquery"):jQuery)}(function(e){function n(e){return u.raw?e:encodeURIComponent(e)}function o(e){return u.raw?e:decodeURIComponent(e)}function i(e){return n(u.json?JSON.stringify(e):String(e))}function r(e){0===e.indexOf('"')&&(e=e.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return e=decodeURIComponent(e.replace(c," ")),u.json?JSON.parse(e):e}catch(n){}}function t(n,o){var i=u.raw?n:r(n);return e.isFunction(o)?o(i):i}var c=/\+/g,u=e.cookie=function(r,c,f){if(void 0!==c&&!e.isFunction(c)){if(f=e.extend({},u.defaults,f),"number"==typeof f.expires){var a=f.expires,d=f.expires=new Date;d.setTime(+d+864e5*a)}return document.cookie=[n(r),"=",i(c),f.expires?"; Expires="+f.expires.toUTCString():"",f.path?"; Path="+f.path:"",f.domain?"; Domain="+f.domain:"",f.secure?"; secure":""].join("")}for(var s=r?void 0:{},p=document.cookie?document.cookie.split("; "):[],m=0,x=p.length;x>m;m++){var v=p[m].split("="),k=o(v.shift()),l=v.join("=");if(r&&r===k){s=t(l,c);break}r||void 0===(l=t(l))||(s[k]=l)}return s};u.defaults={},e.removeCookie=function(n,o){return void 0===e.cookie(n)?!1:(e.cookie(n,"",e.extend({},o,{expires:-1})),!e.cookie(n))}});

/* jQuery doTimeout: Like setTimeout, but better! - v1.0 - 3/3/2010 http://benalman.com/projects/jquery-dotimeout-plugin/ Copyright (c) 2010 "Cowboy" Ben Alman Dual licensed under the MIT and GPL licenses. http://benalman.com/about/license/ */
(function(jQuery){var a={},c="doTimeout",d=Array.prototype.slice;jQuery[c]=function(){return b.apply(window,[0].concat(d.call(arguments)))};jQuery.fn[c]=function(){var f=d.call(arguments),e=b.apply(this,[c+f[0]].concat(f));return typeof f[0]==="number"||typeof f[1]==="number"?this:e};function b(l){var m=this,h,k={},g=l?jQuery.fn:jQuery,n=arguments,i=4,f=n[1],j=n[2],p=n[3];if(typeof f!=="string"){i--;f=l=0;j=n[1];p=n[2]}if(l){h=m.eq(0);h.data(l,k=h.data(l)||{})}else{if(f){k=a[f]||(a[f]={})}}k.id&&clearTimeout(k.id);delete k.id;function e(){if(l){h.removeData(l)}else{if(f){delete a[f]}}}function o(){k.id=setTimeout(function(){k.fn()},j)}if(p){k.fn=function(q){if(typeof p==="string"){p=g[p]}p.apply(m,d.call(n,i))===true&&!q?o():e()};o()}else{if(k.fn){j===undefined?e():k.fn(j===false);return true}else{e()}}}})(jQuery);

/* Modernizr 2.8.3 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-input-inputtypes-forms_placeholder-load
 */
;window.Modernizr=function(a,b,c){function u(a){i.cssText=a}function v(a,b){return u(prefixes.join(a+";")+(b||""))}function w(a,b){return typeof a===b}function x(a,b){return!!~(""+a).indexOf(b)}function y(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:w(f,"function")?f.bind(d||b):f}return!1}function z(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)o[c[d]]=c[d]in j;return o.list&&(o.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),o}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),e.inputtypes=function(a){for(var d=0,e,g,h,i=a.length;d<i;d++)j.setAttribute("type",g=a[d]),e=j.type!=="text",e&&(j.value=k,j.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(g)&&j.style.WebkitAppearance!==c?(f.appendChild(j),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(j,null).WebkitAppearance!=="textfield"&&j.offsetHeight!==0,f.removeChild(j)):/^(search|tel)$/.test(g)||(/^(url|email)$/.test(g)?e=j.checkValidity&&j.checkValidity()===!1:e=j.value!=k)),n[a[d]]=!!e;return n}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.8.3",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j=b.createElement("input"),k=":)",l={}.toString,m={},n={},o={},p=[],q=p.slice,r,s={}.hasOwnProperty,t;!w(s,"undefined")&&!w(s.call,"undefined")?t=function(a,b){return s.call(a,b)}:t=function(a,b){return b in a&&w(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=q.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(q.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(q.call(arguments)))};return e});for(var A in m)t(m,A)&&(r=A.toLowerCase(),e[r]=m[A](),p.push((e[r]?"":"no-")+r));return e.input||z(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)t(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},u(""),h=j=null,e._version=d,e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))},Modernizr.addTest("placeholder",function(){return"placeholder"in(Modernizr.input||document.createElement("input"))&&"placeholder"in(Modernizr.textarea||document.createElement("textarea"))});

Modernizr.load([{
	test: Modernizr.input.placeholder,
	nope: ['/js/polyfill/placeholders.min.js']
}]);

/* Tooltipster v3.2.6 */
;(function(e,t,n){function s(t,n){this.bodyOverflowX;this.callbacks={hide:[],show:[]};this.checkInterval=null;this.Content;this.$el=e(t);this.$elProxy;this.elProxyPosition;this.enabled=true;this.options=e.extend({},i,n);this.mouseIsOverProxy=false;this.namespace="tooltipster-"+Math.round(Math.random()*1e5);this.Status="hidden";this.timerHide=null;this.timerShow=null;this.$tooltip;this.options.iconTheme=this.options.iconTheme.replace(".","");this.options.theme=this.options.theme.replace(".","");this._init()}function o(t,n){var r=true;e.each(t,function(e,i){if(typeof n[e]==="undefined"||t[e]!==n[e]){r=false;return false}});return r}function f(){return!a&&u}function l(){var e=n.body||n.documentElement,t=e.style,r="transition";if(typeof t[r]=="string"){return true}v=["Moz","Webkit","Khtml","O","ms"],r=r.charAt(0).toUpperCase()+r.substr(1);for(var i=0;i<v.length;i++){if(typeof t[v[i]+r]=="string"){return true}}return false}var r="tooltipster",i={animation:"fade",arrow:true,arrowColor:"",autoClose:true,content:null,contentAsHTML:false,contentCloning:true,debug:true,delay:200,minWidth:0,maxWidth:null,functionInit:function(e,t){},functionBefore:function(e,t){t()},functionReady:function(e,t){},functionAfter:function(e){},icon:"(?)",iconCloning:true,iconDesktop:false,iconTouch:false,iconTheme:"tooltipster-icon",interactive:false,interactiveTolerance:350,multiple:false,offsetX:0,offsetY:0,onlyOne:false,position:"top",positionTracker:false,speed:350,timer:0,theme:"tooltipster-default",touchDevices:true,trigger:"hover",updateAnimation:true};s.prototype={_init:function(){var t=this;if(n.querySelector){if(t.options.content!==null){t._content_set(t.options.content)}else{var r=t.$el.attr("title");if(typeof r==="undefined")r=null;t._content_set(r)}var i=t.options.functionInit.call(t.$el,t.$el,t.Content);if(typeof i!=="undefined")t._content_set(i);t.$el.removeAttr("title").addClass("tooltipstered");if(!u&&t.options.iconDesktop||u&&t.options.iconTouch){if(typeof t.options.icon==="string"){t.$elProxy=e('<span class="'+t.options.iconTheme+'"></span>');t.$elProxy.text(t.options.icon)}else{if(t.options.iconCloning)t.$elProxy=t.options.icon.clone(true);else t.$elProxy=t.options.icon}t.$elProxy.insertAfter(t.$el)}else{t.$elProxy=t.$el}if(t.options.trigger=="hover"){t.$elProxy.on("mouseenter."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=true;t._show()}}).on("mouseleave."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=false}});if(u&&t.options.touchDevices){t.$elProxy.on("touchstart."+t.namespace,function(){t._showNow()})}}else if(t.options.trigger=="click"){t.$elProxy.on("click."+t.namespace,function(){if(!f()||t.options.touchDevices){t._show()}})}}},_show:function(){var e=this;if(e.Status!="shown"&&e.Status!="appearing"){if(e.options.delay){e.timerShow=setTimeout(function(){if(e.options.trigger=="click"||e.options.trigger=="hover"&&e.mouseIsOverProxy){e._showNow()}},e.options.delay)}else e._showNow()}},_showNow:function(n){var r=this;r.options.functionBefore.call(r.$el,r.$el,function(){if(r.enabled&&r.Content!==null){if(n)r.callbacks.show.push(n);r.callbacks.hide=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;if(r.options.onlyOne){e(".tooltipstered").not(r.$el).each(function(t,n){var r=e(n),i=r.data("tooltipster-ns");e.each(i,function(e,t){var n=r.data(t),i=n.status(),s=n.option("autoClose");if(i!=="hidden"&&i!=="disappearing"&&s){n.hide()}})})}var i=function(){r.Status="shown";e.each(r.callbacks.show,function(e,t){t.call(r.$el)});r.callbacks.show=[]};if(r.Status!=="hidden"){var s=0;if(r.Status==="disappearing"){r.Status="appearing";if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-"+r.options.animation+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.stop().fadeIn(i)}}else if(r.Status==="shown"){i()}}else{r.Status="appearing";var s=r.options.speed;r.bodyOverflowX=e("body").css("overflow-x");e("body").css("overflow-x","hidden");var o="tooltipster-"+r.options.animation,a="-webkit-transition-duration: "+r.options.speed+"ms; -webkit-animation-duration: "+r.options.speed+"ms; -moz-transition-duration: "+r.options.speed+"ms; -moz-animation-duration: "+r.options.speed+"ms; -o-transition-duration: "+r.options.speed+"ms; -o-animation-duration: "+r.options.speed+"ms; -ms-transition-duration: "+r.options.speed+"ms; -ms-animation-duration: "+r.options.speed+"ms; transition-duration: "+r.options.speed+"ms; animation-duration: "+r.options.speed+"ms;",f=r.options.minWidth?"min-width:"+Math.round(r.options.minWidth)+"px;":"",c=r.options.maxWidth?"max-width:"+Math.round(r.options.maxWidth)+"px;":"",h=r.options.interactive?"pointer-events: auto;":"";r.$tooltip=e('<div class="tooltipster-base '+r.options.theme+'" style="'+f+" "+c+" "+h+" "+a+'"><div class="tooltipster-content"></div></div>');if(l())r.$tooltip.addClass(o);r._content_insert();r.$tooltip.appendTo("body");r.reposition();r.options.functionReady.call(r.$el,r.$el,r.$tooltip);if(l()){r.$tooltip.addClass(o+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.css("display","none").fadeIn(r.options.speed,i)}r._interval_set();e(t).on("scroll."+r.namespace+" resize."+r.namespace,function(){r.reposition()});if(r.options.autoClose){e("body").off("."+r.namespace);if(r.options.trigger=="hover"){if(u){setTimeout(function(){e("body").on("touchstart."+r.namespace,function(){r.hide()})},0)}if(r.options.interactive){if(u){r.$tooltip.on("touchstart."+r.namespace,function(e){e.stopPropagation()})}var p=null;r.$elProxy.add(r.$tooltip).on("mouseleave."+r.namespace+"-autoClose",function(){clearTimeout(p);p=setTimeout(function(){r.hide()},r.options.interactiveTolerance)}).on("mouseenter."+r.namespace+"-autoClose",function(){clearTimeout(p)})}else{r.$elProxy.on("mouseleave."+r.namespace+"-autoClose",function(){r.hide()})}}else if(r.options.trigger=="click"){setTimeout(function(){e("body").on("click."+r.namespace+" touchstart."+r.namespace,function(){r.hide()})},0);if(r.options.interactive){r.$tooltip.on("click."+r.namespace+" touchstart."+r.namespace,function(e){e.stopPropagation()})}}}}if(r.options.timer>0){r.timerHide=setTimeout(function(){r.timerHide=null;r.hide()},r.options.timer+s)}}})},_interval_set:function(){var t=this;t.checkInterval=setInterval(function(){if(e("body").find(t.$el).length===0||e("body").find(t.$elProxy).length===0||t.Status=="hidden"||e("body").find(t.$tooltip).length===0){if(t.Status=="shown"||t.Status=="appearing")t.hide();t._interval_cancel()}else{if(t.options.positionTracker){var n=t._repositionInfo(t.$elProxy),r=false;if(o(n.dimension,t.elProxyPosition.dimension)){if(t.$elProxy.css("position")==="fixed"){if(o(n.position,t.elProxyPosition.position))r=true}else{if(o(n.offset,t.elProxyPosition.offset))r=true}}if(!r){t.reposition()}}}},200)},_interval_cancel:function(){clearInterval(this.checkInterval);this.checkInterval=null},_content_set:function(e){if(typeof e==="object"&&e!==null&&this.options.contentCloning){e=e.clone(true)}this.Content=e},_content_insert:function(){var e=this,t=this.$tooltip.find(".tooltipster-content");if(typeof e.Content==="string"&&!e.options.contentAsHTML){t.text(e.Content)}else{t.empty().append(e.Content)}},_update:function(e){var t=this;t._content_set(e);if(t.Content!==null){if(t.Status!=="hidden"){t._content_insert();t.reposition();if(t.options.updateAnimation){if(l()){t.$tooltip.css({width:"","-webkit-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-moz-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-o-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-ms-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms",transition:"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms"}).addClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!="hidden"){t.$tooltip.removeClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!=="hidden"){t.$tooltip.css({"-webkit-transition":t.options.speed+"ms","-moz-transition":t.options.speed+"ms","-o-transition":t.options.speed+"ms","-ms-transition":t.options.speed+"ms",transition:t.options.speed+"ms"})}},t.options.speed)}},t.options.speed)}else{t.$tooltip.fadeTo(t.options.speed,.5,function(){if(t.Status!="hidden"){t.$tooltip.fadeTo(t.options.speed,1)}})}}}}else{t.hide()}},_repositionInfo:function(e){return{dimension:{height:e.outerHeight(false),width:e.outerWidth(false)},offset:e.offset(),position:{left:parseInt(e.css("left")),top:parseInt(e.css("top"))}}},hide:function(n){var r=this;if(n)r.callbacks.hide.push(n);r.callbacks.show=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;var i=function(){e.each(r.callbacks.hide,function(e,t){t.call(r.$el)});r.callbacks.hide=[]};if(r.Status=="shown"||r.Status=="appearing"){r.Status="disappearing";var s=function(){r.Status="hidden";if(typeof r.Content=="object"&&r.Content!==null){r.Content.detach()}r.$tooltip.remove();r.$tooltip=null;e(t).off("."+r.namespace);e("body").off("."+r.namespace).css("overflow-x",r.bodyOverflowX);e("body").off("."+r.namespace);r.$elProxy.off("."+r.namespace+"-autoClose");r.options.functionAfter.call(r.$el,r.$el);i()};if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-"+r.options.animation+"-show").addClass("tooltipster-dying");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(s)}else{r.$tooltip.stop().fadeOut(r.options.speed,s)}}else if(r.Status=="hidden"){i()}return r},show:function(e){this._showNow(e);return this},update:function(e){return this.content(e)},content:function(e){if(typeof e==="undefined"){return this.Content}else{this._update(e);return this}},reposition:function(){var n=this;if(e("body").find(n.$tooltip).length!==0){n.$tooltip.css("width","");n.elProxyPosition=n._repositionInfo(n.$elProxy);var r=null,i=e(t).width(),s=n.elProxyPosition,o=n.$tooltip.outerWidth(false),u=n.$tooltip.innerWidth()+1,a=n.$tooltip.outerHeight(false);if(n.$elProxy.is("area")){var f=n.$elProxy.attr("shape"),l=n.$elProxy.parent().attr("name"),c=e('img[usemap="#'+l+'"]'),h=c.offset().left,p=c.offset().top,d=n.$elProxy.attr("coords")!==undefined?n.$elProxy.attr("coords").split(","):undefined;if(f=="circle"){var v=parseInt(d[0]),m=parseInt(d[1]),g=parseInt(d[2]);s.dimension.height=g*2;s.dimension.width=g*2;s.offset.top=p+m-g;s.offset.left=h+v-g}else if(f=="rect"){var v=parseInt(d[0]),m=parseInt(d[1]),y=parseInt(d[2]),b=parseInt(d[3]);s.dimension.height=b-m;s.dimension.width=y-v;s.offset.top=p+m;s.offset.left=h+v}else if(f=="poly"){var w=[],E=[],S=0,x=0,T=0,N=0,C="even";for(var k=0;k<d.length;k++){var L=parseInt(d[k]);if(C=="even"){if(L>T){T=L;if(k===0){S=T}}if(L<S){S=L}C="odd"}else{if(L>N){N=L;if(k==1){x=N}}if(L<x){x=L}C="even"}}s.dimension.height=N-x;s.dimension.width=T-S;s.offset.top=p+x;s.offset.left=h+S}else{s.dimension.height=c.outerHeight(false);s.dimension.width=c.outerWidth(false);s.offset.top=p;s.offset.left=h}}var A=0,O=0,M=0,_=parseInt(n.options.offsetY),D=parseInt(n.options.offsetX),P=n.options.position;function H(){var n=e(t).scrollLeft();if(A-n<0){r=A-n;A=n}if(A+o-n>i){r=A-(i+n-o);A=i+n-o}}function B(n,r){if(s.offset.top-e(t).scrollTop()-a-_-12<0&&r.indexOf("top")>-1){P=n}if(s.offset.top+s.dimension.height+a+12+_>e(t).scrollTop()+e(t).height()&&r.indexOf("bottom")>-1){P=n;M=s.offset.top-a-_-12}}if(P=="top"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left+D-j/2;M=s.offset.top-a-_-12;H();B("bottom","top")}if(P=="top-left"){A=s.offset.left+D;M=s.offset.top-a-_-12;H();B("bottom-left","top-left")}if(P=="top-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top-a-_-12;H();B("bottom-right","top-right")}if(P=="bottom"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left-j/2+D;M=s.offset.top+s.dimension.height+_+12;H();B("top","bottom")}if(P=="bottom-left"){A=s.offset.left+D;M=s.offset.top+s.dimension.height+_+12;H();B("top-left","bottom-left")}if(P=="bottom-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top+s.dimension.height+_+12;H();B("top-right","bottom-right")}if(P=="left"){A=s.offset.left-D-o-12;O=s.offset.left+D+s.dimension.width+12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A<0&&O+o>i){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=o+A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);A=s.offset.left-D-q-12-I;F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A<0){A=s.offset.left+D+s.dimension.width+12;r="left"}}if(P=="right"){A=s.offset.left+D+s.dimension.width+12;O=s.offset.left-D-o-12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A+o>i&&O<0){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=i-A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A+o>i){A=s.offset.left-D-o-12;r="right"}}if(n.options.arrow){var R="tooltipster-arrow-"+P;if(n.options.arrowColor.length<1){var U=n.$tooltip.css("background-color")}else{var U=n.options.arrowColor}if(!r){r=""}else if(r=="left"){R="tooltipster-arrow-right";r=""}else if(r=="right"){R="tooltipster-arrow-left";r=""}else{r="left:"+Math.round(r)+"px;"}if(P=="top"||P=="top-left"||P=="top-right"){var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}else if(P=="bottom"||P=="bottom-left"||P=="bottom-right"){var z=parseFloat(n.$tooltip.css("border-top-width")),W=n.$tooltip.css("border-top-color")}else if(P=="left"){var z=parseFloat(n.$tooltip.css("border-right-width")),W=n.$tooltip.css("border-right-color")}else if(P=="right"){var z=parseFloat(n.$tooltip.css("border-left-width")),W=n.$tooltip.css("border-left-color")}else{var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}if(z>1){z++}var X="";if(z!==0){var V="",J="border-color: "+W+";";if(R.indexOf("bottom")!==-1){V="margin-top: -"+Math.round(z)+"px;"}else if(R.indexOf("top")!==-1){V="margin-bottom: -"+Math.round(z)+"px;"}else if(R.indexOf("left")!==-1){V="margin-right: -"+Math.round(z)+"px;"}else if(R.indexOf("right")!==-1){V="margin-left: -"+Math.round(z)+"px;"}X='<span class="tooltipster-arrow-border" style="'+V+" "+J+';"></span>'}n.$tooltip.find(".tooltipster-arrow").remove();var K='<div class="'+R+' tooltipster-arrow" style="'+r+'">'+X+'<span style="border-color:'+U+';"></span></div>';n.$tooltip.append(K)}n.$tooltip.css({top:Math.round(M)+"px",left:Math.round(A)+"px"})}return n},enable:function(){this.enabled=true;return this},disable:function(){this.hide();this.enabled=false;return this},destroy:function(){var t=this;t.hide();if(t.$el[0]!==t.$elProxy[0])t.$elProxy.remove();t.$el.removeData(t.namespace).off("."+t.namespace);var n=t.$el.data("tooltipster-ns");if(n.length===1){var r=typeof t.Content==="string"?t.Content:e("<div></div>").append(t.Content).html();t.$el.removeClass("tooltipstered").attr("title",r).removeData(t.namespace).removeData("tooltipster-ns").off("."+t.namespace)}else{n=e.grep(n,function(e,n){return e!==t.namespace});t.$el.data("tooltipster-ns",n)}return t},elementIcon:function(){return this.$el[0]!==this.$elProxy[0]?this.$elProxy[0]:undefined},elementTooltip:function(){return this.$tooltip?this.$tooltip[0]:undefined},option:function(e,t){if(typeof t=="undefined")return this.options[e];else{this.options[e]=t;return this}},status:function(){return this.Status}};e.fn[r]=function(){var t=arguments;if(this.length===0){if(typeof t[0]==="string"){var n=true;switch(t[0]){case"setDefaults":e.extend(i,t[1]);break;default:n=false;break}if(n)return true;else return this}else{return this}}else{if(typeof t[0]==="string"){var r="#*$~&";this.each(function(){var n=e(this).data("tooltipster-ns"),i=n?e(this).data(n[0]):null;if(i){if(typeof i[t[0]]==="function"){var s=i[t[0]](t[1],t[2])}else{throw new Error('Unknown method .tooltipster("'+t[0]+'")')}if(s!==i){r=s;return false}}else{throw new Error("You called Tooltipster's \""+t[0]+'" method on an uninitialized element')}});return r!=="#*$~&"?r:this}else{var o=[],u=t[0]&&typeof t[0].multiple!=="undefined",a=u&&t[0].multiple||!u&&i.multiple,f=t[0]&&typeof t[0].debug!=="undefined",l=f&&t[0].debug||!f&&i.debug;this.each(function(){var n=false,r=e(this).data("tooltipster-ns"),i=null;if(!r){n=true}else if(a){n=true}else if(l){console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.')}if(n){i=new s(this,t[0]);if(!r)r=[];r.push(i.namespace);e(this).data("tooltipster-ns",r);e(this).data(i.namespace,i)}o.push(i)});if(a)return o;else return this}}};var u=!!("ontouchstart"in t);var a=false;e("body").one("mousemove",function(){a=true})})(jQuery,window,document);

jQuery(document).ready(function() {

	jQuery(".Tooltip").each(function() {
		jQuery(this).prev().tooltipster({
			animation: "fade",
			delay: 50,
			interactive: true,
			contentAsHTML: true,
			position: "bottom",
			content: jQuery('<div class="'+jQuery(this).attr("class")+'" style="'+jQuery(this).attr("style")+'">'+jQuery(this).html()+'</div>')
		});
	});

	jQuery("#SocialLinks a").click(function() {
		ga('send','event','Social Media Button','Click','Footer');
	});
	
	jQuery("#ShoppingCartIndicator").hover(
		function() {
			if (jQuery(document).width() < 669) return;
			jQuery('#CartItemPreview').show();
			ga('send','event','Cart Preview','Rollover','Header');
		}, function() {
			if (jQuery(document).width() < 669) return;
			jQuery('#CartItemPreview').hide();
		}
	);
	
	jQuery('#CartItemPreview .btn-remove').live("click",function() {
		jQuery.get(jQuery(this).attr('href'), function(data) {
			if (!window.location.origin)
				 window.location.origin = window.location.protocol+"//"+window.location.host;
			jQuery.get(window.location.origin+'/productwizard',function(data) {
				jQuery('#CartItemPreview').remove();
				jQuery('#ShoppingCartIndicator').append('<div id="CartItemPreview" style="display: none;">'+data+'</div>');
				var itemQty = 0;
				jQuery('#CartItemPreview .ProductQuantityAndPrice strong').each(function() {
					itemQty += parseInt(jQuery(this).text().trim());
				});
				jQuery("#CurrentCartCount").text(itemQty);
			});
		});
		return false;
	});
	
	jQuery("#MobileBrowseButton").click(function() {
		if (jQuery("#MobileNavigation:visible").length > 0) {
			jQuery("#MobileNavigation").slideUp("fast");
		}
		else {
			jQuery("#MobileNavigation").slideDown("fast");
		}
	});
	
	jQuery(".QuickAddToCartButton").click(function() {
		var _nExistingQuantity = parseInt(jQuery("#CurrentCartCount").text());
		var _nQuantityToAdd = parseInt(jQuery(this).parent().find(".Quantity").val());

		if (jQuery("body").hasClass("catalog-product-view")) {
			ga('send','event','Add To Cart Button','Click','Product Page');
		}
		else {
			ga('send','event','Add To Cart Button','Click','Store Page');
		}

		jQuery.post(jQuery(this).attr("data"),{backurl:location.href,qty:_nQuantityToAdd},function(msg){
			if (jQuery(".AddedToCartMessage").length == 0) {
				jQuery('<div class="AddedToCartMessage"></div>').insertBefore("#FreeShippingMessage");
			}
			if (msg == "added") {
                if (!window.location.origin)
                     window.location.origin = window.location.protocol+"//"+window.location.host;
			    jQuery.get(window.location.origin + '/productwizard', function(data){
			        jQuery('#CartItemPreview').remove();
			        jQuery('#ShoppingCartIndicator').append('<div id="CartItemPreview" style="display: none;">' + data + '</div>');
			    });
				jQuery("#CurrentCartCount").text(_nExistingQuantity+_nQuantityToAdd);
				jQuery(".AddedToCartMessage").html("Added to Cart Successfully!").removeClass("CartError").slideDown("fast").delay(2500).slideUp("fast");
			}
			else {
				jQuery(".AddedToCartMessage").addClass("CartError").html(msg).slideDown("fast").delay(5000).slideUp("fast");
			}
		});
	});
	
	function vRepositionIntroImageText() {
		if (jQuery("#IntroImage").length > 0) {
			jQuery("#IntroImage h1").css("padding-top",(jQuery("#IntroImage").height()-jQuery("#IntroImage h1").height())/2+"px");
		}
	}
	vRepositionIntroImageText();
	jQuery(window).resize(function() {
		vRepositionIntroImageText();
	});
	
	jQuery("#NewsletterForm button").click(function() {
		ga('send','event','Newsletter Signup','Submit','Footer');
		jQuery.ajax({
			url: "https://www.hilinecoffee.com/CaptureNewsletterEmail.php",
			data: {"email":jQuery("#NewsletterForm input").val()},
			success: function(data)
			{
				jQuery("#NewsletterForm p").html(data);
			},
			cache: false
		});
	});
				
	jQuery('.Popup .ClosePopup,.Popup .MiniClosePopup').live("click",function() {
		ga('send','event','Close Popup Button','Click');
		jQuery(".Popup,#PopupBackground").hide();
	});
	
	jQuery(".HiddenContentToggle").click(function() {
		if (jQuery(".HiddenContent:visible").length > 0) {
			jQuery(".HiddenContent").slideUp("fast");
		}
		else {
			jQuery(".HiddenContent").slideDown("fast");
		}
		return false;
	});
	
	jQuery(".HiddenContentToggle").hover(function() {
		jQuery(".HiddenContent").slideDown("fast");
	},function() {
		jQuery(".HiddenContent").slideUp("fast");
	});
	
	jQuery("#SubmitEmailCapture").click(function() {
		jQuery.ajax({
			url: "https://www.hilinecoffee.com/EmailCapture.php",
			data: {
				"List":jQuery("#EmailCaptureList").val(),
				"EmailRecipient":jQuery("#EmailRecipient").val(),
			},
			success: function(data)
			{
				jQuery("#SubmitResponse").html(data).show("fast");
			},
			cache: false
		});
	});

	if (jQuery("body").hasClass("cms-pages-nespresso-vertuoline-review")
		|| jQuery("body").hasClass("cms-pages-best-nespresso-machine")) {
	
		jQuery.doTimeout("ShowPixieGiveawaySignupPopup",10000,function(){ ShowPixieGiveawaySignupPopup(); });
		function ShowPixieGiveawaySignupPopup()
		{
			if (!jQuery.cookie("HasSeenPixieGiveawaySignupPopup")) {
				jQuery.cookie("HasSeenPixieGiveawaySignupPopup",true,{expires:365});
				
				jQuery(".wrapper").append('<div id="PopupBackground">\
					<div id="Popup" class="Popup">\
						<div class="MiniClosePopup"><img src="https://store-static.hilinecoffee.com/blogs/news/wp-content/uploads/CancelButton_White.png"/></div>\
						<h3>\
							Enter to win a\
							<strong>Nespresso Pixie</strong>\
						</h3>\
						<p id="PopupPara1"><input placeholder="Email address" type="text"><button class="GreenButton" type="button">GO!</button></p>\
						<p id="PopupPara2"></p>\
						<div class="ClosePopup"><img src="https://store-static.hilinecoffee.com/blogs/news/wp-content/uploads/CancelButton_White.png"/><span>Close</span></div>\
					</div>\
				</div>');
				
				jQuery('#PopupPara1 button').bind({
					click: function() {
						ga('send','event','Popup Newsletter Signup','Submit','Pixie Giveaway');
						jQuery.ajax({
							url: "https://www.hilinecoffee.com/CaptureNewsletterEmail.php",
							data: {"email":jQuery("#PopupPara1 input").val(),"list":"pixie_giveaway"},
							success: function(data)
							{
								jQuery("#PopupPara2").show().html(data);
							},
							cache: false
						});
					}
				});
			}
		}
	}
	
	function ShowNewsletterSignupPopup(p_sList)
	{
		if (!jQuery.cookie("HasSeenNewsletterSignupPopup")) {
			jQuery.cookie("HasSeenNewsletterSignupPopup",true,{expires:365});

			jQuery(".wrapper").append('<div id="PopupBackground">\
				<div id="NewsletterPopup" class="Popup">\
					<div class="MiniClosePopup"><img src="https://store-static.hilinecoffee.com/blogs/news/wp-content/uploads/popup_close.png"/></div>\
					<h3>Save 10%</h3>\
					<p id="PopupPara1">on your next HiLineCoffee.com order when you join our email list.</p>\
					<p id="PopupPara2">Plus, be the first to hear about new products, exclusive offers and more.</p>\
					<p id="PopupPara3"><input placeholder="Email address" type="text"><button class="GreenButton" type="button">GO!</button></p>\
					<p id="PopupPara4"></p>\
					<div class="ClosePopup"><img src="https://store-static.hilinecoffee.com/blogs/news/wp-content/uploads/popup_close.png"/><span>Close</span></div>\
				</div>\
			</div>');

			jQuery('#PopupPara3 button').bind({
				click: function() {
					ga('send','event','Popup Newsletter Signup','Submit','Home Page');
					jQuery.ajax({
						url: "https://www.hilinecoffee.com/CaptureNewsletterEmail.php",
						data: {"email":jQuery("#PopupPara3 input").val(),"list":p_sList},
						success: function(data)
						{
							jQuery("#PopupPara4").show().html(data);
						},
						cache: false
					});
				}
			});
		}
	}
	
	function ShowWholeBeanFreeShippingPopup()
	{
		if (!jQuery.cookie("HasSeenWholeBeanFreeShippingPopup")) {
			jQuery.cookie("HasSeenWholeBeanFreeShippingPopup",true,{expires:365});

			jQuery(".wrapper").append('<div id="PopupBackground">\
				<div id="WholeBeanFreeShippingPopup" class="Popup">\
					<div class="MiniClosePopup"><img src="https://store-static.hilinecoffee.com/blogs/news/wp-content/uploads/popup_close.png"/></div>\
					<h3>Free Shipping</h3>\
					<p id="PopupPara1">Please enjoy free shipping with any whole bean coffee purchase. At checkout, use code <b>FSBEAN</b></p>\
					<div class="GreyButton ClosePopup">Ok, got it!</div>\
				</div>\
			</div>');
		}
	}
	
	if (jQuery("body").hasClass("cms-shop-whole-bean-coffee")
		|| jQuery("body").hasClass("cms-freshly-roasted-coffee-beans")) {
	
			//jQuery.doTimeout("ShowNewsletterSignupPopup",5000,function(){ ShowWholeBeanFreeShippingPopup(); });
	}
	
	if (jQuery("body").hasClass("cms-pages-alternative-to-keurig-kcups")
		|| jQuery("body").hasClass("cms-pages-keurig-coffee-makers")
		|| jQuery("body").hasClass("cms-pages-alternative-to-k-cups-coffee")
		|| jQuery("body").hasClass("cms-pages-alternative-to-keurig-coffee-pods")
		|| jQuery("body").hasClass("cms-alternative-to-green-mountain-coffee")
		|| jQuery("body").hasClass("cms-organic-coffee-beans")
		|| jQuery("body").hasClass("cms-coffee-subscription")
		|| jQuery("body").hasClass("cms-pourover-chemex-coffee")
		|| jQuery("body").hasClass("cms-french-press-coffee")
		|| jQuery("body").hasClass("cms-aeropress-coffee")
		|| jQuery("body").hasClass("cms-moka-pot-coffee")
		|| jQuery("body").hasClass("cms-espresso-coffee")
		|| jQuery("body").hasClass("cms-pages-best-nespresso-machine")
		|| jQuery("body").hasClass("cms-pages-nespresso-coupon")
		|| jQuery("body").hasClass("cms-pages-nespresso-nyc")
		|| jQuery("body").hasClass("cms-pages-nespresso-pods")
		|| jQuery("body").hasClass("cms-pages-nespresso-club")) {
	
		jQuery.doTimeout("ShowNewsletterSignupPopup",10000,function(){ ShowNewsletterSignupPopup("cms_page"); });
	}
	
	if (jQuery("body").hasClass("cms-home")) {
	
		jQuery.doTimeout("ShowNewsletterSignupPopup",10000,function(){ ShowNewsletterSignupPopup("homepage"); });
		
		

		jQuery("#ShowMoreToggle").click(function() {
			ga('send','event','Show/Hide Hidden Paragraphs Button','Click','Home Page');
			jQuery("#ShowMoreWrapper").slideToggle("fast");
		});
		
		jQuery("#BackToTopButton").click(function() {
			jQuery("html,body").animate({
				scrollTop: 0
			},500);
		});
		
		var g_bBackToTopButtonVisible = false;
		jQuery(window).scroll(function(){
			if (jQuery("html,body").scrollTop() < 500) {
				if (g_bBackToTopButtonVisible) {
					g_bBackToTopButtonVisible = false;
					jQuery("#BackToTopButton").fadeOut("fast");
				}
			}
			else {
				if (!g_bBackToTopButtonVisible) {
					g_bBackToTopButtonVisible = true;
					jQuery("#BackToTopButton").fadeIn("fast");
				}
			}
		});
	}

	if (jQuery("body").hasClass("cms-pages-faq")) {
		jQuery("#accordion").accordion({
			 heightStyle: "content"
		});

		jQuery("#accordion h3").click(function() {
			jQuery("html,body").animate({
				scrollTop: jQuery("#accordion").offset().top
			},500);
		});

	}

	if (jQuery("body").hasClass("cms-shop-ground-coffee-packs")) {
		jQuery(".ProductGrid li").each(function(){
			var _sProductUrl = jQuery(this).find("a").attr("href");
			jQuery(this).find(".Prices").after('<a href="'+_sProductUrl+'" class="GreenButton">shop</a>');
		});
	}

	if (jQuery("body").hasClass("cms-shop-accessories")) {
		jQuery(".ProductGrid .Product54 .Prices").after('<a href="'+jQuery(".ProductGrid .Product54 a").attr("href")+'" class="GreenButton">select</a>');
		jQuery(".ProductGrid .Product55 .Prices").after('<a href="'+jQuery(".ProductGrid .Product55 a").attr("href")+'" class="GreenButton">select</a>');
		jQuery(".ProductGrid .Product56 .Prices").after('<a href="'+jQuery(".ProductGrid .Product56 a").attr("href")+'" class="GreenButton">select</a>');
	}
	
	if (jQuery("body").hasClass("cms-collections-shop")) {
		
		function vSelectCategory(p_sCategoryName) {
			ga('send','event','Product Category','Click','Shop Page');
			jQuery("#AllProducts .ProductGrid").hide();
			jQuery("#"+p_sCategoryName).show();
			jQuery("#ProductCategories ."+p_sCategoryName).addClass("Selected");
		}
		
		if (jQuery.cookie("LastViewedShopCategory")) {
			vSelectCategory(jQuery.cookie("LastViewedShopCategory"));
		}
		else {
			vSelectCategory("DarkRoasts");
		}
		
		jQuery("#ProductCategories li").click(function() {
			if (jQuery(this).attr("class") == "Disclaimer") return;
			jQuery("#ProductCategories .Selected").removeClass("Selected");
			vSelectCategory(jQuery(this).attr("class"));
		});
		
		jQuery("#AllProducts div").click(function() {
			jQuery.cookie("LastViewedShopCategory",jQuery(this).attr("id"));
		});
	}
	
	
	if (jQuery("body").hasClass("cms-shop-ground-coffee-packs")) {

		jQuery("#HowItWorksToggle").click(function() {
			jQuery("#HowItWorksWrapper").slideToggle("slow");
		});
	}
	
	if (jQuery("body").hasClass("product-park-ave-dark-roast")
		|| jQuery("body").hasClass("product-tribeca-dark-roast")
		|| jQuery("body").hasClass("product-empire-state-dark-roast")) {
		jQuery("#attribute148 option").each(function(){
			if (jQuery(this).html() == "Coarse") {
				jQuery("#attribute148").append(jQuery(this).clone().html("French Press"));
				jQuery("#attribute148").append(jQuery(this).clone().html("Chemex"));
				jQuery("#attribute148").append(jQuery(this).clone().html("Drip"));
				jQuery("#attribute148").append(jQuery(this).clone().html("Percolator"));
				jQuery(this).remove();
			}
			if (jQuery(this).html() == "Fine") {
				jQuery("#attribute148").append(jQuery(this).clone().html("Moka Pot"));
				jQuery("#attribute148").append(jQuery(this).clone().html("AeroPress"));
				jQuery(this).remove();
			}
		});
		
		jQuery("#product-options-wrapper label").html("Select your coffee maker:");
	}
	
	if (jQuery("body").hasClass("cms-we-love-startups")) {
		
		jQuery('<div style="text-align:center;">\
			<form id="StartupSignupForm">\
				<h3>Tell us about your startup:</h3><br/>\
				<p></p>\
				<fieldset>\
					<input type="text" name="_sYourName" placeholder="Your Name"/><br/>\
					<input type="text" name="_sYourEmail" placeholder="Your Email"/><br/>\
					<input type="text" name="_sCompanyName" placeholder="Company Name"/><br/>\
					<input type="text" name="_sCompanyWebsite" placeholder="Company Website"/><br/>\
					<input type="text" name="_sHowDidYouHearAboutThis" placeholder="How did you hear about this?"/><br/>\
					<button type="button" class="GreenButton">Submit</button>\
				</fieldset>\
			</form>\
			<style type="text/css">\
				#StartupSignupForm { display:inline-block; text-align:center; margin:2em auto 0 auto; }\
				#StartupSignupForm h3 { display:inline-block; text-transform:uppercase; letter-spacing:0.1em; }\
				#StartupSignupForm p { display:none; }\
				#StartupSignupForm fieldset { border:0;display:inline-block; text-align:left; }\
				#StartupSignupForm fieldset input { width:260px; margin-bottom:0.5em; }\
			</style>\
		</div>').insertAfter("#Row2");
		jQuery("#StartupSignupForm button").click(function () {
			jQuery.ajax({
				url: "https://www.hilinecoffee.com/CaptureStartupInfo.php",
				data: {
					"_sYourName":jQuery("#StartupSignupForm [name=_sYourName]").val(),
					"_sYourEmail":jQuery("#StartupSignupForm [name=_sYourEmail]").val(),
					"_sCompanyName":jQuery("#StartupSignupForm [name=_sCompanyName]").val(),
					"_sCompanyWebsite":jQuery("#StartupSignupForm [name=_sCompanyWebsite]").val(),
					"_sHowDidYouHearAboutThis":jQuery("#StartupSignupForm [name=_sHowDidYouHearAboutThis]").val()
				},
				success: function(data)
				{
					jQuery("#StartupSignupForm p").html(data).slideDown();
				},
				cache: false
			});
		});
	}
	
	if (jQuery("body").hasClass("catalog-product-view")) {

		function vPositionProductDetails() {
			if (jQuery(window).width() < 670) {
				if (jQuery(".Column1 #ImportantLinks").length > 0) {
					jQuery(".Column1 #ImportantLinks").insertAfter(".Column2 blockquote");
				}
				if (jQuery(".Column1 #Description").length > 0) {
					jQuery(".Column1 #Description").insertAfter(".Column2 blockquote");
				}
				if (jQuery(".Column1 #ShortDescription").length > 0) {
					jQuery(".Column1 #ShortDescription").insertAfter(".Column2 blockquote");
				}
			}
			else {
				if (jQuery(".Column2 #ImportantLinks").length > 0) {
					jQuery(".Column2 #ImportantLinks").insertAfter(".Column1 h3");
				}
				if (jQuery(".Column2 #Description").length > 0) {
					jQuery(".Column2 #Description").insertAfter(".Column1 h3");
				}
				if (jQuery(".Column2 #ShortDescription").length > 0) {
					jQuery(".Column2 #ShortDescription").insertAfter(".Column1 h3");
				}
			}
		}
		vPositionProductDetails();
		jQuery(window).resize(function() {
			vPositionProductDetails();
		});

		jQuery("#ImageGallery .Thumbnail").click(function() {
			ga('send','event','Product Thumbnail','Click','Product Page');
			jQuery("#MainImage").attr("src",jQuery(this).attr("data-fullsize"));
		});

		jQuery('#product_addtocart_form').submit(function(e) {

			var _nExistingQuantity = parseInt(jQuery("#CurrentCartCount").text());
			var _nQuantityToAdd = parseInt(jQuery("#qty").val());

			e.preventDefault();
			var data = jQuery(this).serialize();
			data += '&backurl=' + location.href + '&redirect=';
			jQuery.post(jQuery(this).attr("action"), data, function(msg){
				if (msg == "added") {
					if (!window.location.origin)
						 window.location.origin = window.location.protocol+"//"+window.location.host;
					jQuery.get(window.location.origin + '/productwizard', function(data){
						jQuery('#CartItemPreview').remove();
						jQuery('#ShoppingCartIndicator').append('<div id="CartItemPreview" style="display: none;">' + data + '</div>');
					});
					jQuery("#CurrentCartCount").text(_nExistingQuantity+_nQuantityToAdd);
					if (jQuery(".AddedToCartMessage").length == 0) {
						jQuery('<div class="AddedToCartMessage">Added to Cart Successfully!</div>').insertBefore("#FreeShippingMessage");
					}
					jQuery(".AddedToCartMessage").slideDown("fast").delay(2500).slideUp("fast");
				}
			});
			return false;
		});
	}

	function vSelectPlan(p_sPlanId) {
		vResetQuantities();
		jQuery("#PlanSelectionStep .Plan.Selected").removeClass("Selected GreenBorder").addClass("GreyBorder")
			.find("h3").removeClass("GreenLabel").addClass("GreyLabel");
		jQuery("#"+p_sPlanId).removeClass("GreyBorder").addClass("Selected GreenBorder")
			.find("h3").removeClass("GreyLabel").addClass("GreenLabel");
		jQuery("#BlendSelectionStep #QuantityIndicator").removeClass("RequiredQuantityMet");
		jQuery("#BlendSelectionStep #QuantityIndicator .RequiredQuantity").html(jQuery("#"+p_sPlanId).attr("data-RequiredQuantity"));
		jQuery("#BlendSelectionStep #QuantityIndicator #AdditionalQuantityRequired").html("");
		jQuery("#BlendSelectionStep .Blend").hide();
		jQuery("#BlendSelectionStep .Blend."+jQuery("#"+p_sPlanId).attr("data-BrewMethod")).show();
	}

	function vResetQuantities() {
		jQuery('#SubmitPlanButton').hide();
		jQuery('#BlendSelectionStep .CurrentItemQuantity').each(function(){
			jQuery(this).html("0");
		});
		jQuery('#BlendSelectionStep #CurrentTotalQuantity').html("0");
		jQuery("#BlendSelectionStep #SubscribableItems li").removeClass("Selected");
		jQuery("#BlendSelectionStep #SubscribableItems li .GreenButton").removeClass("GreenButton").addClass("GreyButton");
		jQuery("#BlendSelectionStep #SubscribableItems li .GreenLabel").removeClass("GreenLabel").addClass("GreyLabel");
	}

	function vAlterBlendQuantity(p_sBlendId,p_sMethod) {
		if (jQuery("#"+p_sBlendId).length == 0) return;
		_iCurrentTotalQuantity = parseInt(jQuery("#CurrentTotalQuantity").html());
		_iRequiredQuantity = parseInt(jQuery("#PlanSelectionStep .Selected").attr("data-RequiredQuantity"));
		_iCurrentItemQuantity = parseInt(jQuery("#"+p_sBlendId).find(".CurrentItemQuantity").html());
		_iCapsulesPerBox = parseInt(jQuery("#"+p_sBlendId).find(".CapsulesPerBox").val());
		_iBoxesPerSelection = parseInt(jQuery("#"+p_sBlendId).find(".BoxesPerSelection").val());
		if (p_sMethod == "decrement") {
			if (_iCurrentItemQuantity == 0) return;
			_iNewCurrentItemQuantity = _iCurrentItemQuantity-_iCapsulesPerBox*_iBoxesPerSelection;
			_iNewCurrentTotalItemQuantity = _iCurrentTotalQuantity-_iCapsulesPerBox*_iBoxesPerSelection;
			jQuery("#"+p_sBlendId).find(".CurrentItemQuantity").html(_iNewCurrentItemQuantity);
			jQuery("#CurrentTotalQuantity").html(_iCurrentTotalQuantity-_iCapsulesPerBox*_iBoxesPerSelection);
		}
		else { // increment
			if (_iCurrentTotalQuantity == _iRequiredQuantity
				|| _iCurrentTotalQuantity+_iCapsulesPerBox*_iBoxesPerSelection > _iRequiredQuantity) return;
			_iNewCurrentItemQuantity = _iCurrentItemQuantity+_iCapsulesPerBox*_iBoxesPerSelection;
			_iNewCurrentTotalItemQuantity = _iCurrentTotalQuantity+_iCapsulesPerBox*_iBoxesPerSelection;
			jQuery("#"+p_sBlendId).find(".CurrentItemQuantity").html(_iNewCurrentItemQuantity);
			jQuery("#CurrentTotalQuantity").html(_iCurrentTotalQuantity+_iCapsulesPerBox*_iBoxesPerSelection);
		}
		if (_iNewCurrentItemQuantity > 0) {
			jQuery("#"+p_sBlendId).addClass("Selected");
			jQuery("#"+p_sBlendId).find(".GreyButton").removeClass("GreyButton").addClass("GreenButton");
			jQuery("#"+p_sBlendId).find(".GreyLabel").removeClass("GreyLabel").addClass("GreenLabel");
		}
		else if (_iNewCurrentItemQuantity == 0) {
			jQuery("#"+p_sBlendId).removeClass("Selected");
			jQuery("#"+p_sBlendId).find(".GreenButton").removeClass("GreenButton").addClass("GreyButton");
			jQuery("#"+p_sBlendId).find(".GreenLabel").removeClass("GreenLabel").addClass("GreyLabel");
		}

		if (_iRequiredQuantity-_iNewCurrentTotalItemQuantity > 0) {
			jQuery('#SubmitPlanButton').slideUp("slow");
			jQuery("#QuantityIndicator").removeClass("RequiredQuantityMet");
			jQuery("#AdditionalQuantityRequired").html(", please select "+(_iRequiredQuantity-_iNewCurrentTotalItemQuantity)+" more!");
		}
		else {
			jQuery('#SubmitPlanButton').slideDown("slow");
			jQuery("#QuantityIndicator").addClass("RequiredQuantityMet");
			jQuery("#AdditionalQuantityRequired").html(", you're good to go!");
		}
	}

	if (jQuery("body").hasClass("cms-edit-subscription")) {
		jQuery('#PlanSelectionStep .ConfigurePlanButton').click(function(){
			if (!jQuery(this).parent().hasClass("Selected")) {
				vSelectPlan(jQuery(this).parent().attr("id"));
			}
		});
	}

	if (jQuery("body").hasClass("cms-fresh-pods-for-keurig-business-subscription2")
		|| jQuery("body").hasClass("cms-fresh-pods-for-nespresso-business-subscription2")
		|| jQuery("body").hasClass("cms-fresh-pods-for-keurig-subscription2")
		|| jQuery("body").hasClass("cms-fresh-pods-for-nespresso-subscription2")
		|| jQuery("body").hasClass("cms-kcup-variety-pack-sub-test")
		|| jQuery("body").hasClass("cms-nespresso-subscribe-consumer-2")) {
	
		mixpanel.track("Reached subscription selection page");

		jQuery('#PlanSelectionStep .ConfigurePlanButton').click(function(){
			mixpanel.track("Selected subscription plan",{"PlanId":jQuery(this).parent().attr("data-PlanProductId")});
			if (jQuery(this).attr("data-BlendSelectionMode") == "automatic") {
				_aProductIds = new Array();
				_aProductIds.push(jQuery(this).parent().attr("data-PlanProductId"));
				jQuery("#PlanForm input").val(_aProductIds.join(","));
				jQuery("#PlanForm").submit();
				return;
			}
			if (!jQuery(this).parent().hasClass("Selected")) {
				vSelectPlan(jQuery(this).parent().attr("id"));
			}
			jQuery("#BlendSelectionStep").slideDown("slow");
			jQuery("#PlanSelectionStep").slideUp("slow");
			jQuery("html,body").animate({
				scrollTop: jQuery("#IntroImage").next().offset().top
			},500);
		});
		
		jQuery('#SubmitPlanButton button').click(function(){
			mixpanel.track("Finished subscription blend selection");
		});

		jQuery('#BackToPlanSelectionStep').click(function(){
			jQuery("#BlendSelectionStep").slideUp("slow");
			jQuery("#PlanSelectionStep").slideDown("slow");
		});
	}

	if (jQuery("body").hasClass("cms-fresh-pods-for-keurig-business-subscription2")
		|| jQuery("body").hasClass("cms-fresh-pods-for-nespresso-business-subscription2")
		|| jQuery("body").hasClass("cms-fresh-pods-for-keurig-subscription2")
		|| jQuery("body").hasClass("cms-fresh-pods-for-nespresso-subscription2")
		|| jQuery("body").hasClass("cms-kcup-variety-pack-sub-test")
		|| jQuery("body").hasClass("cms-nespresso-subscribe-consumer-2")) {
		jQuery('#BlendSelectionStep .QuantityModifierButton').click(function(){
			if (jQuery(this).hasClass("DecrementQuantity")) {
				vAlterBlendQuantity(jQuery(this).parent().attr("id"),"decrement");
			}
			else { 
				vAlterBlendQuantity(jQuery(this).parent().attr("id"),"increment");
			}
		});
		
		jQuery('#SubmitPlanButton button').click(function() {
			_aProductIds = new Array();
			_aProductIds.push(jQuery("#PlanSelectionStep .Selected").attr("data-PlanProductId"));
			jQuery(".CurrentItemQuantity").each(function(){
				_mQuantity = parseInt(jQuery(this).html())/parseInt(jQuery(this).parent().find(".CapsulesPerBox").val());
				for (i=0;i<_mQuantity;++i) {
					_aProductIds.push(jQuery(this).parent().find(".ProductId").val());
				}
			});
			jQuery("#PlanForm input").val(_aProductIds.join(","));
			jQuery("#PlanForm").submit();
		});
		
		if (typeof g_sPreviouslySelectedPlan !== 'undefined') {
			vSelectPlan(g_sPreviouslySelectedPlan);
			jQuery.each(g_aSelectedBlends,function(key,p_nBlendId) {
				vAlterBlendQuantity("Blend"+p_nBlendId,"increment");
			});
		}
	}
	
	if (jQuery("body").hasClass("cms-edit-subscription")) {
		jQuery('#BlendSelectionStep .QuantityModifierButton').click(function(){
			if (jQuery(this).hasClass("DecrementQuantity")) {
				vAlterBlendQuantity(jQuery(this).parent().attr("id"),"decrement");
			}
			else { 
				vAlterBlendQuantity(jQuery(this).parent().attr("id"),"increment");
			}
		});
		
		jQuery('#SubmitPlanButton button').click(function() {
			_aPlanBlends = new Array();
			_aPlanBlends.push(jQuery("#PlanSelectionStep .Selected").attr("data-PlanProductId"));
			jQuery(".CurrentItemQuantity").each(function(){
				_mQuantity = parseInt(jQuery(this).html())/parseInt(jQuery(this).parent().find(".CapsulesPerBox").val());
				for (i=0;i<_mQuantity;++i) {
					_aPlanBlends.push(jQuery(this).parent().find(".ProductId").val());
				}
			});
			jQuery("#PlanForm").append('<input type="hidden" name="PlanId" value="'+jQuery("#PlanSelectionStep .Selected").attr("data-PlanProductId")+'"/>'
				+'<input type="hidden" name="PlanBlends" value="'+_aPlanBlends.join(",")+'"/>');
			jQuery("#PlanForm").submit();
		});
		
		if (typeof g_sPreviouslySelectedPlan !== 'undefined') {
			vSelectPlan(g_sPreviouslySelectedPlan);
			jQuery.each(g_aSelectedBlends,function(key,p_nBlendId) {
				vAlterBlendQuantity("Blend"+p_nBlendId,"increment");
			});
		}
	}
	
});